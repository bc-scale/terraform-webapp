SHELL = /bin/sh
export ROOTS_VERSION=master

export COMPANY_NAME=SkaleSys
export COMPANY_WEBSITE=https://skalesys.com
export COMPANY_DOMAIN=skalesys.com

export LOGO_THEME=technology,web

export PROJECT_NAME=terraform-webapp
export REGION=ap-southeast-2

export BUCKET=sk-prd-terraform-state
export DYNAMODB_TABLE=sk-prd-terraform-state-lock

-include $(shell [ ! -d .roots ] && git clone --branch $(ROOTS_VERSION) https://gitlab.com/skalesys/roots.git .roots; echo .roots/Makefile)

.PHONY: install
## Install project requirements
install:
	@make --silent terraform/install
	@make --silent gomplate/install

# Region needs to be us-east-1 to avoid issues with certificate generation
# Reference: https://aws.amazon.com/premiumsupport/knowledge-center/custom-ssl-certificate-cloudfront/

.PHONY: certificate/new
## Use the AWS cli to request new ACM certifiates available globally (us-east-1) (requires email validation, check SPAM folder)
certificate/global:
	@( \
		source $(ROOTS_VENV_PATH)/bin/activate && \
		aws --region=us-east-1 acm request-certificate --domain-name $(COMPANY_DOMAIN) --subject-alternative-names www.$(COMPANY_DOMAIN) *.dev.$(COMPANY_DOMAIN) *.tst.$(COMPANY_DOMAIN) *.stg.$(COMPANY_DOMAIN) *.$(COMPANY_DOMAIN) \
	)

.PHONY: certificate/new
## Use the AWS cli to request new ACM certifiates available at $(REGION) (requires email validation, check SPAM folder)
certificate/regional:
	@( \
		source $(ROOTS_VENV_PATH)/bin/activate && \
		aws --region=$(REGION) acm request-certificate --domain-name $(COMPANY_DOMAIN) --subject-alternative-names www.$(COMPANY_DOMAIN) *.dev.$(COMPANY_DOMAIN) *.tst.$(COMPANY_DOMAIN) *.stg.$(COMPANY_DOMAIN) *.$(COMPANY_DOMAIN) \
	)