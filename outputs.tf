#-----
# MAIN
#-----

output "region" {
  value = "${var.region}"
}

output "account_id" {
  value = "${data.aws_caller_identity.current.account_id}"
}

output "user_id" {
  value = "${data.aws_caller_identity.current.user_id}"
}

output "arn" {
  value = "${data.aws_caller_identity.current.arn}"
}

#---
# S3
#---

output "statements_bucket_id" {
  description = "The name of the bucket."

  value = "${aws_s3_bucket.statements.id}"
}

output "statements_bucket_arn" {
  description = "The ARN of the bucket. Will be of format arn:aws:s3:::bucketname."

  value = "${aws_s3_bucket.statements.arn}"
}

output "statements_bucket_domain_name" {
  description = "The bucket domain name. Will be of format bucketname.s3.amazonaws.com."

  value = "${aws_s3_bucket.statements.bucket_domain_name}"
}

output "statements_bucket_regional_domain_name" {
  description = "The bucket region-specific domain name. The bucket domain name including the region name, please refer here for format. Note: The AWS CloudFront allows specifying S3 region-specific endpoint when creating S3 origin, it will prevent redirect issues from CloudFront to S3 Origin URL."

  value = "${aws_s3_bucket.statements.bucket_regional_domain_name}"
}

output "statements_bucket_hosted_zone_id" {
  description = "The Route 53 Hosted Zone ID for this bucket's region."

  value = "${aws_s3_bucket.statements.hosted_zone_id}"
}

#----
# ALB
#----

output "frontend_alb_access_logs_bucket_id" {
  description = "The S3 bucket ID for access logs"

  value = "${module.frontend_alb.access_logs_bucket_id}"
}

output "frontend_alb_arn" {
  description = "The ARN of the ALB"

  value = "${module.frontend_alb.alb_arn}"
}

output "frontend_alb_arn_suffix" {
  description = "The ARN suffix of the ALB"

  value = "${module.frontend_alb.alb_arn_suffix}"
}

output "frontend_alb_dns_name" {
  description = "DNS name of ALB"

  value = "${module.frontend_alb.alb_dns_name}"
}

output "frontend_alb_name" {
  description = "The ARN suffix of the ALB"

  value = "${module.frontend_alb.alb_name}"
}

output "frontend_alb_zone_id" {
  description = "The ID of the zone which ALB is provisioned"

  value = "${module.frontend_alb.alb_zone_id}"
}

output "frontend_alb_default_target_group_arn" {
  description = "The default target group ARN"

  value = "${module.frontend_alb.default_target_group_arn}"
}

output "frontend_alb_http_listener_arn" {
  description = "The ARN of the HTTP listener"

  value = "${module.frontend_alb.http_listener_arn}"
}

output "frontend_alb_https_listener_arn" {
  description = "The ARN of the HTTPS listener"

  value = "${module.frontend_alb.https_listener_arn}"
}

output "frontend_alb_listener_arns" {
  description = "A list of all the listener ARNs"

  value = "${module.frontend_alb.listener_arns}"
}

output "frontend_alb_security_group_id" {
  description = "The security group ID of the ALB"

  value = "${module.frontend_alb.security_group_id}"
}

output "frontend_autoscaling_group_arn" {
  description = "The ARN for this AutoScaling Group"

  value = "${module.frontend_asg_group.autoscaling_group_arn}"
}

output "frontend_autoscaling_group_default_cooldown" {
  description = "Time between a scaling activity and the succeeding scaling activity"

  value = "${module.frontend_asg_group.autoscaling_group_default_cooldown}"
}

output "frontend_autoscaling_group_desired_capacity" {
  description = "The number of Amazon EC2 instances that should be running in the group"

  value = "${module.frontend_asg_group.autoscaling_group_desired_capacity}"
}

output "frontend_autoscaling_group_health_check_grace_period" {
  description = "Time after instance comes into service before checking health"

  value = "${module.frontend_asg_group.autoscaling_group_health_check_grace_period}"
}

output "frontend_autoscaling_group_health_check_type" {
  description = "EC2 or ELB. Controls how health checking is done"

  value = "${module.frontend_asg_group.autoscaling_group_health_check_type}"
}

output "frontend_autoscaling_group_id" {
  description = "The autoscaling group id"

  value = "${module.frontend_asg_group.autoscaling_group_id}"
}

output "frontend_autoscaling_group_max_size" {
  description = "The maximum size of the autoscale group"

  value = "${module.frontend_asg_group.autoscaling_group_max_size}"
}

output "frontend_autoscaling_group_min_size" {
  description = "The minimum size of the autoscale group"

  value = "${module.frontend_asg_group.autoscaling_group_min_size}"
}

output "frontend_autoscaling_group_name" {
  description = "The autoscaling group name"

  value = "${module.frontend_asg_group.autoscaling_group_name}"
}

output "frontend_launch_template_arn" {
  description = "The ARN of the launch template"

  value = "${module.frontend_asg_group.launch_template_arn}"
}

output "frontend_launch_template_id" {
  description = "The ID of the launch template"

  value = "${module.frontend_asg_group.launch_template_id}"
}

output "backend_autoscaling_group_arn" {
  description = "The ARN for this AutoScaling Group"

  value = "${module.backend_asg_group.autoscaling_group_arn}"
}

output "backend_autoscaling_group_default_cooldown" {
  description = "Time between a scaling activity and the succeeding scaling activity"

  value = "${module.backend_asg_group.autoscaling_group_default_cooldown}"
}

output "backend_autoscaling_group_desired_capacity" {
  description = "The number of Amazon EC2 instances that should be running in the group"

  value = "${module.backend_asg_group.autoscaling_group_desired_capacity}"
}

output "backend_autoscaling_group_health_check_grace_period" {
  description = "Time after instance comes into service before checking health"

  value = "${module.backend_asg_group.autoscaling_group_health_check_grace_period}"
}

output "backend_autoscaling_group_health_check_type" {
  description = "EC2 or ELB. Controls how health checking is done"

  value = "${module.backend_asg_group.autoscaling_group_health_check_type}"
}

output "backend_autoscaling_group_id" {
  description = "The autoscaling group id"

  value = "${module.backend_asg_group.autoscaling_group_id}"
}

output "backend_autoscaling_group_max_size" {
  description = "The maximum size of the autoscale group"

  value = "${module.backend_asg_group.autoscaling_group_max_size}"
}

output "backend_autoscaling_group_min_size" {
  description = "The minimum size of the autoscale group"

  value = "${module.backend_asg_group.autoscaling_group_min_size}"
}

output "backend_autoscaling_group_name" {
  description = "The autoscaling group name"

  value = "${module.backend_asg_group.autoscaling_group_name}"
}

output "backend_launch_template_arn" {
  description = "The ARN of the launch template"

  value = "${module.backend_asg_group.launch_template_arn}"
}

output "backend_launch_template_id" {
  description = "The ID of the launch template"

  value = "${module.backend_asg_group.launch_template_id}"
}

###

output "backend_alb_access_logs_bucket_id" {
  description = "The S3 bucket ID for access logs"

  value = "${module.backend_alb.access_logs_bucket_id}"
}

output "backend_alb_arn" {
  description = "The ARN of the ALB"

  value = "${module.backend_alb.alb_arn}"
}

output "backend_alb_arn_suffix" {
  description = "The ARN suffix of the ALB"

  value = "${module.backend_alb.alb_arn_suffix}"
}

output "backend_alb_dns_name" {
  description = "DNS name of ALB"

  value = "${module.backend_alb.alb_dns_name}"
}

output "backend_alb_name" {
  description = "The ARN suffix of the ALB"

  value = "${module.backend_alb.alb_name}"
}

output "backend_alb_zone_id" {
  description = "The ID of the zone which ALB is provisioned"

  value = "${module.backend_alb.alb_zone_id}"
}

output "backend_alb_default_target_group_arn" {
  description = "The default target group ARN"

  value = "${module.backend_alb.default_target_group_arn}"
}

output "backend_alb_http_listener_arn" {
  description = "The ARN of the HTTP listener"

  value = "${module.backend_alb.http_listener_arn}"
}

output "backend_alb_https_listener_arn" {
  description = "The ARN of the HTTPS listener"

  value = "${module.backend_alb.https_listener_arn}"
}

output "backend_alb_listener_arns" {
  description = "A list of all the listener ARNs"

  value = "${module.backend_alb.listener_arns}"
}

output "backend_alb_security_group_id" {
  description = "The security group ID of the ALB"

  value = "${module.backend_alb.security_group_id}"
}

#--------
# BASTION
#--------

output "bastion_security_group_id" {
  description = "The ID of the security group"

  value = "${aws_security_group.bastion.id}"
}

output "bastion_security_group_arn" {
  description = "The ARN of the security group"

  value = "${aws_security_group.bastion.arn}"
}

output "bastion_security_group_vpc_id" {
  description = "The VPC ID."

  value = "${aws_security_group.bastion.vpc_id}"
}

output "bastion_security_group_owner_id" {
  description = "The owner ID."

  value = "${aws_security_group.bastion.owner_id}"
}

output "bastion_security_group_name" {
  description = "The name of the security group"

  value = "${aws_security_group.bastion.name}"
}

output "bastion_security_group_description" {
  description = "The description of the security group"

  value = "${aws_security_group.bastion.description}"
}

output "bastion_security_group_ingress" {
  description = "The ingress rules. See above for more."

  value = "${aws_security_group.bastion.ingress}"
}

output "bastion_security_group_egress" {
  description = "The egress rules. See above for more."

  value = "${aws_security_group.bastion.egress}"
}

output "bastion_instance_id" {
  description = "The instance ID."

  value = "${aws_instance.bastion.id}"
}

output "bastion_instance_arn" {
  description = "The ARN of the instance."

  value = "${aws_instance.bastion.arn}"
}

output "bastion_instance_availability_zone" {
  description = "The availability zone of the instance."

  value = "${aws_instance.bastion.availability_zone}"
}

output "bastion_instance_placement_group" {
  description = "The placement group of the instance."

  value = "${aws_instance.bastion.placement_group}"
}

output "bastion_instance_key_name" {
  description = "The key name of the instance"

  value = "${aws_instance.bastion.key_name}"
}

output "bastion_instance_public_dns" {
  description = "The public DNS name assigned to the instance. For EC2-VPC, this is only available if you've enabled DNS hostnames for your VPC"

  value = "${aws_instance.bastion.public_dns}"
}

output "bastion_instance_primary_network_interface_id" {
  description = "The ID of the instance's primary network interface."

  value = "${aws_instance.bastion.primary_network_interface_id}"
}

output "bastion_instance_security_groups" {
  description = "The associated security groups."

  value = "${aws_instance.bastion.security_groups}"
}

output "bastion_instance_vpc_security_group_ids" {
  description = "The associated security groups in non-default VPC"

  value = "${aws_instance.bastion.vpc_security_group_ids}"
}

output "bastion_instance_subnet_id" {
  description = "The VPC subnet ID."

  value = "${aws_instance.bastion.subnet_id}"
}

output "bastion_instance_credit_specification" {
  description = "Credit specification of instance."

  value = "${aws_instance.bastion.credit_specification}"
}

#----
# EC2
#----

output "key_pair_name" {
  description = "Name of SSH key"

  value = "${module.ssh_key_pair.key_name}"
}

output "key_pair_private_key_filename" {
  description = "Private Key Filename"

  value = "${module.ssh_key_pair.private_key_filename}"
}

output "key_pair_public_key" {
  description = "Contents of the generated public key"

  value = "${module.ssh_key_pair.public_key}"
}

output "key_pair_public_key_filename" {
  description = "Public Key Filename"

  value = "${module.ssh_key_pair.public_key_filename}"
}

#----
# RDS
#----

output "db_sg_label" {
  value = {
    id         = "${module.db_sg_label.id}"
    name       = "${module.db_sg_label.name}"
    namespace  = "${module.db_sg_label.namespace}"
    stage      = "${module.db_sg_label.stage}"
    attributes = "${module.db_sg_label.attributes}"
  }
}

output "db_sg_label_tags" {
  value = "${module.db_sg_label.tags}"
}

output "db_sg_label_context" {
  value = "${module.db_sg_label.context}"
}

output "db_label" {
  value = {
    id         = "${module.db_label.id}"
    name       = "${module.db_label.name}"
    namespace  = "${module.db_label.namespace}"
    stage      = "${module.db_label.stage}"
    attributes = "${module.db_label.attributes}"
  }
}

output "db_label_tags" {
  value = "${module.db_label.tags}"
}

output "db_label_context" {
  value = "${module.db_label.context}"
}

output "db_sg_id" {
  description = "The ID of the security group"

  value = "${aws_security_group.database.id}"
}

output "database_arn" {
  description = "The ARN of the security group"

  value = "${aws_security_group.database.arn}"
}

output "database_vpc_id" {
  description = "The VPC ID."

  value = "${aws_security_group.database.vpc_id}"
}

output "database_owner_id" {
  description = "The owner ID."

  value = "${aws_security_group.database.owner_id}"
}

output "database_name" {
  description = "The name of the security group"

  value = "${aws_security_group.database.name}"
}

output "database_description" {
  description = "The description of the security group"

  value = "${aws_security_group.database.description}"
}

output "database_ingress" {
  description = "The ingress rules. See above for more."

  value = "${aws_security_group.database.ingress}"
}

output "database_egress" {
  description = "The egress rules. See above for more."

  value = "${aws_security_group.database.egress}"
}

output "rds_cluster_arn" {
  description = "Amazon Resource Name (ARN) of cluster"

  value = "${module.db_cluster.arn}"
}

output "rds_cluster_cluster_name" {
  description = "Cluster Identifier"

  value = "${module.db_cluster.cluster_name}"
}

output "rds_cluster_cluster_resource_id" {
  description = "The region-unique, immutable identifie of the cluster."

  value = "${module.db_cluster.cluster_resource_id}"
}

output "rds_cluster_dbi_resource_ids" {
  description = "List of the region-unique, immutable identifiers for the DB instances in the cluster."

  value = "${module.db_cluster.dbi_resource_ids}"
}

output "rds_cluster_endpoint" {
  description = "The DNS address of the RDS instance"

  value = "${module.db_cluster.endpoint}"
}

output "rds_cluster_master_host" {
  description = "DB Master hostname"

  value = "${module.db_cluster.master_host}"
}

output "rds_cluster_name" {
  description = "Database name"

  value = "${module.db_cluster.name}"
}

output "rds_cluster_reader_endpoint" {
  description = "A read-only endpoint for the Aurora cluster, automatically load-balanced across replicas"

  value = "${module.db_cluster.reader_endpoint}"
}

output "rds_cluster_replicas_host" {
  description = "Replicas hostname"

  value = "${module.db_cluster.replicas_host}"
}

output "rds_cluster_user" {
  description = "Username for the master DB user"

  value = "${module.db_cluster.user}"
}
